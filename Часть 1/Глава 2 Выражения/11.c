#include <stdio.h>

int main(void)
{
  extern int first, last; /* Глобальные переменные */

  printf("%d %d", first, last);

  return 0;
}

/* Глобально определение переменные */
int first = 10, last = 20;
