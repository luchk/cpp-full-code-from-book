/* Если єта функция является частью программы на языке С,
   при компиляции возкинкет ошибка. Если эта функция
   представляет собой часть программы на языке С++,
   ошибки не будет
*/
void f(void)
{
  int i;

  i = 10;

  int j;  /* Єтот оеператор порождает ошибку */

  j = 20;
}
